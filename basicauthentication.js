//basic authentication
const express = require("express");
const print = require("./print");
const app = express();
const port = "3000";
const host = "localhost";

var html = "<h1>welcome to my page</h1>";


function auth(req,res,next){
	
	var authHeader = req.headers.authorization;
	if(!authHeader){
		var err = new Error("Error:Not authenticated!");
		res.setHeader("WWW-Authenticate","Basic");
		err.status = 401;
		next(err);
		return;
	}
	var auth = new Buffer(authHeader.split(" ")[1],'base64').toString().split(':');
	const user = auth[0];
	const pass = auth[1];
	if(user=="joshua" && pass=="222534"){
		next();
	}else{
		var err = new Error("Error:Not authenticated,Invalid inputs!");
		res.setHeader("WWW-Authenticate","Basic");
		err.status = 401;
		next(err);
	}
}

app.use(auth);

app.all("/",(req,res,next)=>{
	res.setHeader("Content-Type","text/html");
	res.statusCode = 201;
	next();
});

app.get("/",(req,res,next)=>{
	res.end(html);
	next();
});


app.listen(port,host,()=>{
	print("Server running at "+host+":"+port);
});